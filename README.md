# Technical Test

![](./Hands-on-task.jpeg)

##  Requirements:

1) Install [JDK 11](https://www.oracle.com/java/technologies/javase-jdk11-downloads.html)

2) Install [Maven](https://maven.apache.org/download.cgi), [Follow these steps](https://www.baeldung.com/install-maven-on-windows-linux-mac) to configure maven depending your OS

3) Install [Docker](https://docs.docker.com/install/), docker is used to launch (Postgres, Kafka, Mongo and Keycloak Image)

4) Install [Git](https://git-scm.com/downloads) on your machine

## Build the projects with their respective docker images

1) Build the Hub code

  	```
  	mvn clean install
  	```
  	
## Running docker compose (Cassandra, Kafka, API and Processor):

1) Run the Docker compose stack

    ```
    docker-compose up -d
    ```

NB: The Cassandra KeySpace is not automatically created, the API will fail to start the first time the stack is started.

* Open cassandla client CQLSH:

  ```
  docker exec -it cassandra cqlsh -u cassandra -p cassandra
  ```

* Create the keyspace:

  ```
  create KEYSPACE eis WITH replication={'class':'SimpleStrategy', 'replication_factor':1};
  ```

* Exit cassandla client CQLSH:

  ```
  exit
  ```

* Restart the API:

  ```
  docker-compose up -d words-api
  ```


## Using the API:

Lets assume the project is started on the localhost.

1) Post a word
  	```
    curl -d 'aWord' http://localhost:8085/api/v1/words
  	```

2) Post another word
  	```
    curl -d 'anotherWord1' http://localhost:8085/api/v1/words
  	```

3) List the posted words
  	```
    curl http://localhost:8085/api/v1/words
  	```

3) List the composed sentences
  	```
    curl http://localhost:8085/api/v1/sentences
  	```

3) List the composed sentences that start with "Test"
  	```
    curl http://localhost:8085/api/v1/sentences?startWith=Test
  	```





