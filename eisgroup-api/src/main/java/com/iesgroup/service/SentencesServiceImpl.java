package com.iesgroup.service;

import com.iesgroup.model.Sentence;
import com.iesgroup.model.Word;
import com.iesgroup.repo.NonReactiveSentencesRepository;
import com.iesgroup.repo.SentencesRepository;
import org.apache.kafka.clients.consumer.ConsumerRecord;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.kafka.annotation.KafkaListener;
import org.springframework.stereotype.Component;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

import java.util.List;
import java.util.UUID;
import java.util.stream.Collectors;

@Component
public class SentencesServiceImpl implements SentencesService {

    @Autowired
    private SentencesRepository sentencesRepository;

    @Autowired
    private NonReactiveSentencesRepository nonReactiveSentencesRepository;



    @KafkaListener(groupId = "client", topics = "sentences")
    public void listenSignal(ConsumerRecord<String, String> record) {
        Sentence sentence = new Sentence();
        sentence.setId(UUID.randomUUID());
        sentence.setSentence(record.value());
        nonReactiveSentencesRepository.save(sentence);
    }

    public Flux<String> getSentences() {
        return sentencesRepository.findAll()
                .flatMap(s -> Flux.just(s.getSentence()));
    }

    public Flux<String> getSentences(String startWith) {
        return sentencesRepository.findBySentenceStartingWith(startWith)
                .flatMap(s -> Flux.just(s.getSentence()));
    }
}
