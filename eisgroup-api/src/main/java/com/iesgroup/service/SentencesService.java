package com.iesgroup.service;

import reactor.core.publisher.Flux;

import java.util.List;

public interface SentencesService {

    Flux<String> getSentences();

    Flux<String> getSentences(String startWith);
}
