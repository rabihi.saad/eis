package com.iesgroup.service;

import com.iesgroup.model.Word;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

import java.util.List;

public interface WordsService {

    Flux<String> getWords();
    Flux<Word> getWords_();

    Mono<Word> addWord(String w);
}
