package com.iesgroup.service;

import com.iesgroup.model.Word;
import com.iesgroup.repo.WordsRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.kafka.core.KafkaTemplate;
import org.springframework.stereotype.Component;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

import java.util.List;
import java.util.UUID;
import java.util.stream.Collectors;

@Component
public class WordsServiceImpl implements WordsService {

    @Autowired
    private WordsRepository wordsRepository;

    @Autowired
    private KafkaTemplate<String, String> kafkaTemplate;

    public Flux<String> getWords() {
        return wordsRepository.findAll()
                .flatMap(w -> Flux.just(w.getWord()));
    }

    public Flux<Word> getWords_() {
        return wordsRepository.findAll();
    }

    public Mono<Word> addWord(String w) {
        kafkaTemplate.send("words", w);
        Word word = new Word();
        word.setId(UUID.randomUUID());
        word.setWord(w);
        return wordsRepository.save(word);
    }
}
