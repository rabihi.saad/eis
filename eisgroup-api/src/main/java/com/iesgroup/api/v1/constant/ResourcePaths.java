package com.iesgroup.api.v1.constant;

public class ResourcePaths {

    public final static String API_PREFIX = "/api";
    public final static String API_VERSION = "/v1";

    public final static String API_WORDS = API_PREFIX + API_VERSION + "/words";
    public final static String API_SENTENCES = API_PREFIX + API_VERSION + "/sentences";
}
