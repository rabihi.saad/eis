package com.iesgroup.api.v1;

import com.iesgroup.api.v1.constant.ResourcePaths;
import com.iesgroup.service.SentencesService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

import java.util.List;

@RestController
@RequestMapping(ResourcePaths.API_SENTENCES)
public class SentencesResource {

    @Autowired
    private SentencesService sentencesService;

    @GetMapping(produces = MediaType.APPLICATION_JSON_VALUE)
    public Mono<List<String>> getSentences(String startWith) {
        if (startWith != null) {
            return sentencesService.getSentences(startWith).collectList();
        } else {
            return sentencesService.getSentences().collectList();
        }
    }
}
