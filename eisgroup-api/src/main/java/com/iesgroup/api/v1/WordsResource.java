package com.iesgroup.api.v1;

import com.iesgroup.api.v1.constant.ResourcePaths;
import com.iesgroup.model.Word;
import com.iesgroup.service.WordsService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

import java.util.List;

@RestController
@RequestMapping(ResourcePaths.API_WORDS)
public class WordsResource {

    @Autowired
    private WordsService wordsService;

    @GetMapping(produces = MediaType.APPLICATION_JSON_VALUE)
    public Mono<List<String>> getWords() {
        return wordsService.getWords().collectList();
    }

    @PostMapping(produces = MediaType.APPLICATION_JSON_VALUE)
    public Mono<Word> addWord(@RequestBody String w) {
        return wordsService.addWord(w);
    }
}
