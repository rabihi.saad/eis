package com.iesgroup.repo;

import com.iesgroup.model.Sentence;
import org.springframework.data.cassandra.repository.CassandraRepository;
import org.springframework.data.cassandra.repository.ReactiveCassandraRepository;
import reactor.core.publisher.Flux;

import java.util.List;
import java.util.UUID;

public interface SentencesRepository extends ReactiveCassandraRepository<Sentence, UUID> {
    public Flux<Sentence> findBySentenceStartingWith(String s);
}
