package com.iesgroup.repo;

import com.iesgroup.model.Sentence;
import org.springframework.data.cassandra.repository.CassandraRepository;
import org.springframework.data.cassandra.repository.ReactiveCassandraRepository;
import reactor.core.publisher.Flux;

import java.util.UUID;

public interface NonReactiveSentencesRepository extends CassandraRepository<Sentence, UUID> {
}
