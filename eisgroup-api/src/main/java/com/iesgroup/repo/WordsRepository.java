package com.iesgroup.repo;

import com.iesgroup.model.Word;
import org.springframework.data.cassandra.repository.CassandraRepository;
import org.springframework.data.cassandra.repository.ReactiveCassandraRepository;

import java.util.UUID;

public interface WordsRepository extends ReactiveCassandraRepository<Word, UUID> {
}
