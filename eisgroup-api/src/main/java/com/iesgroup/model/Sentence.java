package com.iesgroup.model;

import org.springframework.data.cassandra.core.mapping.Indexed;
import org.springframework.data.cassandra.core.mapping.PrimaryKey;
import org.springframework.data.cassandra.core.mapping.SASI;
import org.springframework.data.cassandra.core.mapping.Table;

import java.util.UUID;

@Table
public class Sentence {
    @PrimaryKey
    private UUID id;

    @Indexed
    @SASI(indexMode = SASI.IndexMode.PREFIX)
    @SASI.StandardAnalyzed
    private String sentence;

    public UUID getId() {
        return id;
    }

    public void setId(UUID id) {
        this.id = id;
    }

    public String getSentence() {
        return sentence;
    }

    public void setSentence(String sentence) {
        this.sentence = sentence;
    }
}
