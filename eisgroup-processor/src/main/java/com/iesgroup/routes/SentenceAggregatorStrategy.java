package com.iesgroup.routes;

import org.apache.camel.Exchange;
import org.apache.camel.processor.aggregate.AggregationStrategy;

public class SentenceAggregatorStrategy implements AggregationStrategy {
    @Override
    public Exchange aggregate(Exchange oldExchange, Exchange newExchange) {
        if (oldExchange == null) {
            return newExchange;
        }
        oldExchange.getIn().setBody(oldExchange.getIn().getBody().toString().concat(" ").concat(newExchange.getIn().getBody().toString()));
        return oldExchange;
    }
}
