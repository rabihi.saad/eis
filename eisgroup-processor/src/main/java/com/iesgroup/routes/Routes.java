package com.iesgroup.routes;

import org.apache.camel.builder.RouteBuilder;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

@Component
public class Routes extends RouteBuilder {

    @Value("${com.iesgroup.routes.sentences.aggregate.timeout}")
    private Integer timeout;

    @Value("${kafka.bootstrapAddress}")
    private String kafkaBootstrapAddress;

    @Override
    public void configure() {
        from("kafka:words?brokers=".concat(kafkaBootstrapAddress).concat("&groupId=processor"))
                .setHeader("aHeader", () -> "aValue") // Its mandatory to group by an expression, this is just to cheat camel
                .aggregate(header("aHeader"), new SentenceAggregatorStrategy()).completionTimeout(timeout)
                .to("kafka:sentences?brokers=".concat(kafkaBootstrapAddress));
    }
}
